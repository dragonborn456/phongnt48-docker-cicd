terraform {
    required_providers {
        docker = {
            source = "kreuzwerker/docker"
            version = "2.13.0"
        } 
    }
}

provider "docker" {
    host = "npipe:////.//pipe//docker_engine"
}

resource "docker_image" "phongnt48-docker" {
    name = "phongnt48-docker"
    build {
        path = "./"
        dockerfile = "Dockerfile"
    }
}

resource "docker_container" "phongnt48-docker" {
    image = docker_image.phongnt48-docker.latest
    name = "phongnt48-docker-container"
    ports {
        internal = 5000
        external = 5000
    }
}